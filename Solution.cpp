#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include "Solution.h"
using namespace std;


Solution::Solution(vector<string> _content){
  content = _content;
}

void Solution::analyze(){
  int size = stoi(content[1]);
  int level = stoi(content[2]);
  map_info.size = size;
  map_info.level = level;
  string checkstr("#ESH");
  int i = 0;
  for (auto it = content.begin()+3;it != content.end(); ++it){
    string linestr(*it);
    if (linestr.substr(0,2)!= "//"){
        int current_level = level - i/size -1;
        for(int j = 0; j < size; j++){
          if (checkstr.find(linestr[j]) != string::npos){
            int row = size - (i - (i/size) * size) - 1;
            cordinate current_cordinate;
            current_cordinate.row = row;
            current_cordinate.column = j;
            current_cordinate.level = current_level;
            if (linestr.substr(j,1) == "#")
              map_info.wall.push_back(current_cordinate);
            else if (linestr.substr(j,1) == "E")
              map_info.elevator.push_back(current_cordinate);
            else if (linestr.substr(j,1) == "S")
              map_info.startpoint = current_cordinate;
            else if (linestr.substr(j,1) == "H")
              map_info.endpoint = current_cordinate;
          }
        }
        i ++;
    }
  }
}

cordinate Solution::getstartpoint(){
  return  map_info.startpoint;
}

cordinate Solution::getendpoint(){
  return  map_info.endpoint;
}

void Solution::debug(){
  cout << "level" << map_info.level << endl;
  cout << "size" << map_info.size << endl;
  cout << "startpoint" << endl;
  out_cordinate(getstartpoint());
  cout << "endpoint" << endl;
  out_cordinate(getendpoint());
  vector<cordinate> elevator = map_info.elevator;
  vector<cordinate> wall = map_info.wall;
  cout << "elevator" << endl;
  for(unsigned int i =0; i<elevator.size();i++){
    out_cordinate(elevator[i]);
  }
  cout << "wall" << endl;
  for(unsigned int i =0; i<wall.size();i++){
    out_cordinate(wall[i]);
  }
}

void Solution::out_cordinate(cordinate _cord){
  cout << _cord.row << _cord.column << _cord.level << endl;
}

int Solution::find_queuesolution(){
  queue<node> my_queue;
  cordinate startpoint = getstartpoint();
  out_cordinate(startpoint);
  node firstnode = {startpoint,""};
  my_queue.push(firstnode);
  while(!my_queue.empty()){
    cordinate current_cord = my_queue.front().cord;
    string current_string = my_queue.front().route;
    vector<pair<cordinate,string>> next_avail_cord = get_nextavail(current_cord);
    if (checkend(next_avail_cord)){
      my_queue.pop();
      string laststep = getenddirection(current_cord);
      string resultstring = current_string + laststep;
      cout << "++++" << endl;
      cout << resultstring << endl;
      return 1;
    }else{
      my_queue.pop();
      poped_context.push_back(current_cord);
      for(auto it = next_avail_cord.begin();it!=next_avail_cord.end();it++){
        string next_string = current_string +it->second;
        node next_node = {it->first,next_string};
        //out_cordinate(next_node.cord);
        my_queue.push(next_node);
        poped_context.push_back(next_node.cord);

        }
    }
  }
  return 0;
}

vector<pair<cordinate,string>> Solution::get_nextavail(cordinate _cord){
  vector<pair<cordinate,string>> near_cords = get_nearcords(_cord);
  vector<pair<cordinate,string>> result;
  result.clear();
  for(auto it=near_cords.begin();it!=near_cords.end();it++){
    if (!(checksize(it->first) || checkwall(it->first) || checkpoped(it->first))){
    result.push_back(pair<cordinate,string>(it->first,it->second));}
  }
  if(checkelevator(_cord)){
    vector<pair<cordinate,string>> elevator_cords = get_elevatorcords(_cord);
    for(auto it= elevator_cords.cbegin();it != elevator_cords.cend();it++){
    result.push_back(pair<cordinate,string>(it->first,it->second));}
  }
  return result;
}

vector<pair<cordinate,string>> Solution::get_nearcords(cordinate _cord){
  vector<pair<cordinate,string>> result;
  result.clear();
  cordinate north_cord(_cord.row+1,_cord.column,_cord.level);
  cordinate east_cord(_cord.row,_cord.column+1,_cord.level);
  cordinate south_cord(_cord.row-1,_cord.column,_cord.level);
  cordinate west_cord(_cord.row,_cord.column-1,_cord.level);
  result.push_back(pair<cordinate,string>(north_cord,"n"));
  result.push_back(pair<cordinate,string>(east_cord,"e"));
  result.push_back(pair<cordinate,string>(south_cord,"s"));
  result.push_back(pair<cordinate,string>(west_cord,"w"));
  return result;
}

vector<pair<cordinate,string>> Solution::get_elevatorcords(cordinate _cord){
  vector<pair<cordinate,string>> result;
  for(unsigned int i = 0; i < map_info.elevator.size(); i++){
    if (_cord.row == map_info.elevator[i].row && _cord.column == map_info.elevator[i].column && _cord.level != map_info.elevator[i].level)
      result.push_back(pair<cordinate,string>(map_info.elevator[i],to_string(map_info.elevator[i].level)));
  }
  // result is sequenced by level high to low;
  return result;
}

string Solution::getenddirection(cordinate _cord){
  cordinate endpoint = getendpoint();
  if (_cord.row - endpoint.row == -1)
    return "n";
  else if (_cord.row - endpoint.row == 1)
    return "s";
  else if (_cord.column - endpoint.column == -1)
    return "e";
  else
    return "w";
}


bool Solution::checkelevator(cordinate _cord){
  for(unsigned int i = 0; i < map_info.elevator.size(); i++){
    if(_cord == map_info.elevator[i])
    return true;
  }
  return false;
}

bool Solution::checksize(cordinate _cord){
  return (!(0<=_cord.row && _cord.row <map_info.size && 0<=_cord.column && _cord.column<map_info.size && 0<=_cord.level && _cord.level<map_info.level));
}

bool Solution::checkwall(cordinate _cord){
  for(unsigned int i = 0; i < map_info.wall.size(); i++){
    if(_cord == map_info.wall[i])
    return true;
  }
  return false;
}

bool Solution::checkpoped(cordinate _cord){
  for(unsigned int i =0; i<poped_context.size(); i++){
    if (_cord == poped_context[i])
    return true;
  }
  return false;
}

bool Solution::checkend(vector<pair<cordinate,string>> _cords){
  cordinate endpoint = getendpoint();
  for(auto it=_cords.begin();it!=_cords.end();it++){
    cordinate current_cordinate = it->first;
    if(current_cordinate == endpoint)
    return true;
  }
  return false;
}

int Solution::find_stacksolution(){
  return 1;
}
