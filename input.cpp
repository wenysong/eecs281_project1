#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include "Solution.h"

using namespace std;



int main(int argc, char** argv){
  vector<string> content;
  if (argc == 2){
    ifstream file(argv[1]);
    string str;
    while(getline(file,str)){
      content.push_back(str);
    }
  }
  Solution* solution = new Solution(content);
  solution->analyze();
  solution->debug();
  cout <<"________________"<< endl;
  solution->find_queuesolution();
  //solution->find_stacksolution();

  return 1;
}
