PATH := /usr/um/gcc-4.7.0/bin:$(PATH)
LD_LIBRARY_PATH := /usr/um/gcc-4.7.0/lib64
LD_RUN_PATH := /usr/um/gcc-4.7.0/lib64


CC=g++
OBJS = input.o Solution.o
CFLAGS=-Wall -Werror -O3
STDFLAG=-std=c++11

project1: $(OBJS)
	${CC} ${CFLAGS} $(OBJS) -o project1 ${STDFLAG}

input.o: input.cpp Solution.h
	${CC} ${CFLAGS} -c input.cpp ${STDFLAG}

Solution.o: Solution.cpp Solution.h
	${CC} ${CFLAGS} -c Solution.cpp ${STDFLAG}

clean:
	rm -f project1 *.o
