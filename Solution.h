#include <string>
#include <vector>

using namespace std;



class cordinate{
public:
  int row;
  int column;
  int level;
  cordinate(int _row=0,int _column=0,int _level=0){row = _row; column = _column; level = _level;}
  bool operator==(const cordinate& b) {
    return (row == b.row && column == b.column && level == b.level);
  }
  cordinate& operator=(const cordinate& b){
    row = b.row;
    column = b.column;
    level = b.level;
    return *this;
  }
};

struct node{
  cordinate cord;
  string route;
};

struct map_content{
  int size;
  int level;
  cordinate startpoint;
  cordinate endpoint;
  vector<cordinate> wall;
  vector<cordinate> elevator;
};


class Solution{
protected:
    vector<string> content;
public:
    map_content map_info;
    vector<cordinate> poped_context;
    Solution(vector<string> _content);
    void analyze();
    void debug();
    cordinate getstartpoint();
    cordinate getendpoint();
    int find_queuesolution();
    vector<pair<cordinate,string>> get_nextavail(cordinate _cord);
    vector<pair<cordinate,string>> get_nearcords(cordinate _cord);
    vector<pair<cordinate,string>> get_elevatorcords(cordinate _cord);
    string getenddirection(cordinate _cord);
    bool checkelevator(cordinate _cord);
    bool checksize(cordinate _cord);
    bool checkwall(cordinate _cord);
    bool checkpoped(cordinate _cord);
    bool checkend(vector<pair<cordinate,string>> _cords);
    int find_stacksolution();
    void out_cordinate(cordinate _cord);
};
